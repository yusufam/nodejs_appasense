const router = require('express').Router()
const { check, validationResult } = require('express-validator/check')
const { matchedData } = require('express-validator/filter')

let getRenderCallback = {
        data: {},
        errors: {}
}

let passJSON = {}
passJSON.errors = {
    message: { msg: 'A message is required', },
    email: { msg: 'That email doesn‘t look right', }
}

let validationRulesList = [
    check('message')
        .isLength({ min: 1 })
        .withMessage('Message is required'),
    check('email')
        .isEmail()
        .withMessage('That email doesn‘t look right')
        .trim()
        .normalizeEmail()
]


router.get('/contact', getCallbackContact)
router.post('/contact', validationRulesList, postCallbackContact)


function getCallbackContact(req, res) {
    res.render('contact', getRenderCallback)
    console.log('In GET contact')
}

function postCallbackContact(req, res) {
    passJSON.data = req.body
    const errors = validationResult(req)
    const data = matchedData(req)
    console.log('Sanitized:', data)

    //If there are errors then
    if (!errors.isEmpty()) {
        passJSON.errors = errors.mapped()
        console.log(passJSON.errors)
        res.render('contact', passJSON)
    } else{
        req.flash('success', 'Thanks for the message! I‘ll be in touch :)')
        res.redirect('/')
    }

    //If the code reaches here that means something is wrong with above conditions.
    //Let the user get stuck on the same page with some error message
    req.flash('error', 'Oops!! System Encountered a failure')
    res.redirect('/contact')

}

module.exports = router


/*    res.render('contact', {
        data: req.body,
        errors: {
            message: {
                msg: 'A message is required',
            },
            email: {
                msg: 'That email doesn‘t look right',
            }
        }
    })
*/
