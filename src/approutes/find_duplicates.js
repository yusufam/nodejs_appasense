// approutes/find_duplicates.js
//IMPORTANT METHODS //
// req.body
// req.query
// req.params.id

let router = {}
var globalsocket = {}
const { check, validationResult } = require('express-validator/check')
const { matchedData } = require('express-validator/filter')
module.exports = function(app, resources) {
  router = resources.routerinstance
  var io = resources.ioinstance
  var server = resources.serverinstance
  var listener = io.listen(server)

  io.on('connection', function (socket) {
    console.log('Connection to client established through find_duplicates');
    globalsocket = socket; //this is done because we need this same socket after python functionality is completed
  })

  let defaultParametersJSON = {
          data: {},
          errors: {},
          socket: {},
  }

  let postValidationRulesList = [
    check('find_duplicates')
      .trim()
      .isLength({ min: 1 })
      .withMessage('No Search Term Provided'),
  ]

//------------ HEREEE IS THE ROUTING START ----------//
  router.get('/find_duplicates', getFindDuplicates)
  router.post('/find_duplicates', postValidationRulesList, postFindDuplicates)

// -------------------- GET ------------------------ //
  function getFindDuplicates(req, res){
    res.render('find_duplicates',defaultParametersJSON)
  }

// -------------------- POST ------------------------ //

// Initialize params that will be required on the post package

  let params = {}
  params.errors = {
    find_duplicates: { msg: 'Search Term was empty', }
  }
  params.data = {}

  function postFindDuplicates(req, res){

      //First call matchedData and then call errors so matched data will create validationResult on match failures
      const data = matchedData(req)
      const errors = validationResult(req)

      console.log('Data Received:', data)
      params.data.find_duplicates = data.find_duplicates


      if (!errors.isEmpty()) {
        //IF VALIDATION ERRORS
        params.errors = errors.mapped()
        console.log('Form submitted with errors on /find_duplicates')
        console.log(params.errors)
        params.data.results = {}
        req.flash('error', 'Oops!! Search Term was invalid')
      }else{
        //IF NO VALIDATION ERRORS
        params.errors = {}
        params.data.find_duplicates = data.find_duplicates
        params.data.results = 'Loading results ... '
        console.log('Form submitted without errors on /find_duplicates')
        req.flash('success', 'Fetching Results ... ')
      /*---------- PERFORM SEARCH --------- */
        performSearch(req, res, data)
      /*-------------------------------------*/
      }
      //This template is rendered with validation data only.
      //The search results will be sent after page is rendered
      res.render('find_duplicates', params)
  }

  const PythonShell    = require('python-shell');
  let pythonOptions = {
    scriptPath: '/home/yusuf/appasense/rpi_relevantsearch',
    pythonPath: '/usr/bin/python3',
  };

  function performSearch(req, res, data) {
    const passData = {
      'index_name':"evg_dev",  //evg_dev is for 10.162.205.175
//      'index_name':"gsam_dev",  // gsam_dev is indexed on localhost
      'component_searchterm':data.find_duplicates,
      'confidence_threshold':60
    }
//We will be passing the parameters as command line arguments to the python script
    const pythonArgs = [passData.index_name,passData.component_searchterm,passData.confidence_threshold]
    pythonOptions.args = pythonArgs

    console.log(passData)
    var returnMessages = []
    var pyshell = new PythonShell('callme1.py',pythonOptions)
    pyshell
      .send(passData.index_name, passData.component_searchterm, passData.confidence_threshold)
      .on('message', (message,err) => {
        // received a message sent from the Python script (a simple "print" statement)
        if(err) {console.log(err);}
        console.log('Message from python : ['+message+']');
        returnMessages.push(message)
      })
      .end(function (err,code,signal) {
        // end the input stream and allow the process to exit
        if (err) throw err;
        console.log('The exit code was: ' + code);
        console.log('The exit signal was: ' + signal);
        console.log('finished');
        console.log('------------PYTHON SCRIPT COMPLETED ITS WORK-------------')
        globalsocket.emit('results', returnMessages[returnMessages.length -1 ])
      })
      console.log('-------------------------------------RETURN performSearch')
      return returnMessages[(returnMessages.length - 1)]
  }
/***********************************************ENDDDD **********************/
  return router
}


//Hints to access or setup logstash
//sudo ln -s /usr/share/logstash/bin/logstash logstash-6.2.2
//Hints for querying Elasticsearch
//------------------ FORMAT ------------------//
// <REST Verb> /<Index>/<Type>/<ID> //
//
// CREATE A NEW Index
//curl -XPUT 'localhost:9200/customer?pretty&pretty'

//GET LIST OF INDICES
//curl -XGET 'localhost:9200/_cat/indices?v&pretty'
//curl -XDELETE 'localhost:9200/customer?pretty&pretty'
//curl -XGET 'localhost:9200/_cat/indices?v&pretty'

//curl -XPUT 'localhost:9200/customer/_doc/1?pretty&pretty' -H 'Content-Type: application/json' -d'
//{
//  "name": "John Doe"
//}
//'
// To install socket.io npm
// NPM SOCKET.IO - https://www.npmjs.com/package/socket.io
//https://stackoverflow.com/questions/30365337/jquery-function-on-click-with-node-js-and-socket-io
