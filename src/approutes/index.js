
//NOTE : Routes will not work until you add them to the array below
//const router = require('express').Router()

//TEST CODE HERE //
const find_duplicates = require('./find_duplicates');
const contact = require('./contact1');

module.exports = function(app, resources) {
  console.log('++++++++++++++++++In approutes/index.js')

  var io = resources.ioinstance
  var server = resources.serverinstance
  var listener = io.listen(server)
  listener.on('connection', function (socket) {
    console.log('Connection to client established');
  })
  io.on('connection', function(socket) {
      console.log(' ++++++++ a user connected through index.js');
      socket.on('join', function(msg){
          console.log('message: ' + msg);
      });
  });

  //Default Route action
  const router = resources.routerinstance
  router.get('/', (req, res) => {
    res.render('index')

    //res.status(200).json({ message: 'Connected!' });
  })


  // Other route groups could go here, in the future
  find_duplicates(app, resources);
  contact(app, resources);
  console.log('++++++++++++++++++OUTT approutes/index.js')
  return resources.routerinstance
};

//TEST CODE HERE //

/*
const registered_routes = ['find_duplicates','contact']

router.get('/', (req, res) => {
  res.render('index')
  console.log('Inside index.js')
  //res.status(200).json({ message: 'Connected!' });
})

registered_routes.map((routename)=>router.use(require(`./${routename}`)))

//Sample Code if above map function fails
//const find_duplicates = require('./find_duplicates')
//router.use(find_duplicates)



module.exports = router;
*/
