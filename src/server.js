
const path = require('path')
const express = require('express')
const app = express()
const router = express.Router()

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

const layout = require('express-layout')
const bodyParser = require('body-parser')
const validator = require('express-validator')
const cookieParser = require('cookie-parser')
const flash = require('express-flash')
const session = require('express-session')
//var http = require('http').Server(app())
const http = require('http')
var server = http.createServer(app)
//var server = app.listen(3000)
const sessionArgs = {
  secret: 'super-secret-key',
  key: 'super-secret-cookie',
  resave: false,
  saveUninitialized: false,
  cookie: { maxAge: 60000 }
}

var io = require('socket.io')(server)
const middlewares = [
  layout(),
  express.static(path.join(__dirname, 'public')),
  bodyParser.urlencoded(),
  validator(),
  cookieParser(),
  flash(),
  session(sessionArgs),
]
app.use(middlewares)

//const routes = require('./routes')
//const routes = require('./approutes')
//app.use('/', routes)

//TEST CODE HERE//
//require('./app/routes')(app, {'PythonShell':PythonShell});



server = app.listen(3000, () => {
  console.log(`App running at http://localhost:3000`)
})


var resources = {'routerinstance':router,
                 'httpinstance':http.Server(app),
                 'ioinstance':io,
                 'serverinstance':server
                }
app.use('/', require('./approutes')(app, resources))
//TEST CODE ABOVE//

app.use((req, res, next) => {
  res.status(404).send("Sorry can't find that!")
})

app.use((err, req, res, next) => {
  console.error(err.stack)
  res.status(500).send('Something broke!')
})
